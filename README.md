# README

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up?

Steps to build the project

1. Clone the repo using git clone https://jamesdjs@bitbucket.org/jamesdjs/assignment1.git
2. Navigate to the index file
3. Open in a browser
4. Enjoy

### License & copyright

Licensed under the [MIT License](LICENSE).

I chose MIT License to promote open source culture :)
